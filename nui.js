var promiseResolve, promiseReject;


$$n = {};
function $n(query){
    if(query == null){
        return $$n;
    }

    var res = Array.from(document.querySelectorAll(query));
    if(res.length == 1)
        return res[0];
    return res;
}

document.addEventListener("click",()=>{
    if($n()._menuGlobal){
        $n()._menuGlobal.close();
        $n()._menuGlobal = null;
    }
});

function contextMenu(div,root){
    var menu = {};
    menu.root = root;
            
    menu.showMenu=(x,y,node)=>{
        console.log("Show Menu ",x,y)
        console.log(node)
        var submenu = {};
        submenu.div = null;
        if(!submenu.div){
            submenu.div = document.createElement("div");
            submenu.div.style.position = "absolute";
            submenu.div.className = "nui_contextMenu";
            submenu.div.style.display = "none";
        }

        submenu.div.style.top = y+"px";
        submenu.div.style.left = x+"px";
        submenu.div.innerHTML = "";
        submenu.div.style.display = "";
        document.body.appendChild(submenu.div);
        submenu.submenus = [];

        node.forEach((elem,k) => {
            if("name" in elem){
                var d = document.createElement("div");
                submenu.div.appendChild(d);
                d.innerText = elem.name;

                if("elems" in elem){
                    d.className+=" nui_contectMenuGroup";
                    d.onclick = function(x,y,elems,e){
                        e.stopPropagation();
                        this.closeSub();
                        this.submenus.push(menu.showMenu(x(),y(),elems));
                    }.bind(submenu,()=>x + submenu.div.clientWidth+3 ,()=> y+d.clientHeight*k , elem.elems)
                }
                
                if("fun" in elem){
                    d.onclick = elem.fun;
                }
            }
        });
        submenu.close = function(){
            this.closeSub();
            this.div.parentElement.removeChild(this.div);
        }
        submenu.closeSub = function(){
            this.submenus.forEach(t=>t.close());
            this.submenus = [];
        }

        return submenu;
    }

    if(typeof(div)=="function"){        
        menu.listener = window.addEventListener("contextmenu", function(e) {
            if(div(e)){
                e.preventDefault();
                console.log($n()._menuGlobal)
                if($n()._menuGlobal != null){
                    $n()._menuGlobal.close();
                    $n()._menuGlobal = null;
                }
                $n()._menuGlobal = menu.showMenu(e.pageX,e.pageY,menu.root);
            }
        })
    }else{
        menu.listener = div.addEventListener("contextmenu",(e)=>{
            e.preventDefault();
            console.log($n()._menuGlobal)
            if($n()._menuGlobal != null){
                $n()._menuGlobal.close();
                $n()._menuGlobal = null;
            }
            $n()._menuGlobal = menu.showMenu(e.pageX,e.pageY,menu.root);
        });
    }


}

function openWindow(config) {
    if (config === undefined) config = {};

    if (!("title" in config)) config.title = "";
    if (!("width" in config)) config.width = "300";
    if (!("cancelText" in config)) config.cancelText = "Abbrechen";
    if (!("okText" in config)) config.okText = "Ok";
    if (!("type" in config)) config.type = "text";
    if (!("autofocus" in config)) config.autofocus = false;
    if (!("multi" in config)) config.multi = [];
    if (!("placeholder" in config)) config.placeholder = "";

    var promiseResolve;
    var promiseReject;
    var multiFields = {};



    var n_popup_submit = ()=>{
        if (config.multi.length > 0) {
            var values = {};
            for (var i = 0; i < config.multi.length; i++) {
                var thisField = config.multi[i];
                if(multiFields[thisField.name]){
                    if("getValue" in multiFields[thisField.name]){
                        values[thisField.name] = multiFields[thisField.name].getValue();
                    }
                    else{
                        values[thisField.name] = multiFields[thisField.name].element.value;
                    }
                }
                    
            }
            promiseResolve(values);
        } else {
            promiseResolve(n_popup_input.value);
        }
        n_popup.remove();
    }

    var n_popup = document.createElement("div");
    n_popup.className = "n_popup";
    n_popup.style.width = config.width+"px";
    n_popup.style.marginLeft = (-parseInt(config.width)/2)+"px";

    var n_popup_t = document.createElement("div");
    n_popup_t.className = "n_popup_t";
    var n_popup_t_text = document.createElement("div");
    n_popup_t_text.className = "n_popup_t_text";
    n_popup_t_text.innerHTML = config.title;
    if (config.multi.length > 0) {
        for (var i = 0; i < config.multi.length; i++) {
            var thisField = config.multi[i];

            if(typeof(thisField)==="string"){
                if(thisField.startsWith("."))
                    thisField = {type:thisField.replace(".","")};
                else
                    thisField = {type:"span",text:thisField}; 
            }

            if (!("name" in thisField)) thisField.name = "field" + i;
            if (!("type" in thisField)) thisField.type = "text";
            if (!("class" in thisField)) thisField.class = "";
            if (!("options" in thisField)) thisField.options = [];
            if (!("placeholder" in thisField)) thisField.placeholder = "";
            if (!("value" in thisField)) thisField.value = "";
            if (!("send" in thisField)) thisField.send = false;
            if (!("styleAppend" in thisField)) thisField.styleAppend = {};

            var element = {};
            var bindObj = {
                field: thisField,
                n_popup: n_popup,
                promiseResolve: (val) => promiseResolve(val),
                promiseReject: (val) => promiseReject(val),
                multiFields: multiFields
            }

            thisField.element = element;
            if (thisField.type == "button") {
                element = document.createElement("button");
                thisField.element = element;
                element.className = "n_popup_input " + thisField.class;
                element.innerHTML = thisField.text;
                n_popup.appendChild(element)
                multiFields[thisField.name] = thisField;
            } else if (thisField.type == "br") {
                element= document.createElement("br");
                thisField.element = element;
                n_popup.appendChild(element);
                multiFields[thisField.name] = thisField;
            } else if (thisField.type == "span") {
                element = document.createElement("span");
                thisField.element = element;
                element.className = "n_popup_input " + thisField.class;
                element.innerHTML = thisField.text;
                n_popup.appendChild(element);
                multiFields[thisField.name] = thisField;
            } else if (thisField.type == "select") {
                element = document.createElement("select");
                thisField.element = element;
                element.className = "n_popup_input " + thisField.class;
                element.type = thisField.type;
                element.placeholder = thisField.placeholder;
                thisField.renewOptions = async function(){
                    console.log("Renew",this)
                    var options = [];
                    if(typeof(this.field.options)==="function"){
                        if(this.field.options.constructor.name === 'AsyncFunction'){
                            options = await this.field.options(this);
                        }else{
                            options = this.field.options(this);
                        }
                    }else{
                        options = this.field.options;
                    }
                    this.field.element.innerHTML = "";
                    for(var option of options){
                        var o = document.createElement("option");
                        o.innerText = (typeof(option)=="object" && "name" in option)?option.name:option;
                        o.value = (typeof(option)=="object" && "value" in option)?option.value:option;
                        this.field.element.appendChild(o);
                    }
                }.bind(bindObj);
                thisField.renewOptions();
                element.value = thisField.value;
                element.onkeyup = function (e) {
                    if(e.which == 13){
                        n_popup_submit();
                    }
                }

                
                multiFields[thisField.name] = thisField;
                n_popup.appendChild(element);
            } else if (thisField.type=="textarea") {
                element = document.createElement("textarea");
                thisField.element = element;
                element.className = "n_popup_input n_ta " + thisField.class;
                element.innerText = thisField.value;
                element.placeholder = thisField.placeholder;
                element = element;        
                multiFields[thisField.name] = thisField;
                n_popup.appendChild(element);
            } else {
                element = document.createElement("input");
                thisField.element = element;
                element.className = "n_popup_input " + thisField.class;
                element.type = thisField.type;
                element.value = thisField.value;
                element.placeholder = thisField.placeholder;
                element = element;
                element.onkeyup = function (e) {
                    if(e.which == 13){
                        n_popup_submit();
                    }
                }
                multiFields[thisField.name] = thisField;
                n_popup.appendChild(element);
            }
            if(element){
                if("onclick" in thisField)
                    element.onclick = thisField.onclick.bind(bindObj);
                if("onchange" in thisField)
                    element.onchange = thisField.onchange.bind(bindObj);
                for(var style_key in thisField.styleAppend){
                    element.style[style_key] = thisField.styleAppend[style_key];
                }
            }
        }
    }

    if (config.okText != "") {
        var n_popup_button_ok = document.createElement("button");
        n_popup_button_ok.className = "n_popup_input";
        n_popup_button_ok.innerHTML = config.okText;
        n_popup_button_ok.onclick = function (e) {
            n_popup_submit();
        }
        n_popup.appendChild(n_popup_button_ok)
    }

    if (config.cancelText != "") {
        var n_popup_button_cancel = document.createElement("button");
        n_popup_button_cancel.className = "n_popup_input";
        n_popup_button_cancel.innerHTML = config.cancelText;
        n_popup_button_cancel.onclick = function (e) {
            promiseReject("cancel");
            n_popup.remove();
        }
        n_popup.appendChild(n_popup_button_cancel)
    }

    n_popup.appendChild(n_popup_t)
    n_popup_t.appendChild(n_popup_t_text)
    document.body.appendChild(n_popup);
    if (config.autofocus) {
        n_popup_input.focus();
    }
    return new Promise(function (resolve, reject) {
        promiseResolve = resolve;
        promiseReject = reject;
    });
}